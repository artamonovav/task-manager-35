package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.IRepository;
import ru.t1.artamonov.tm.api.service.IService;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.IndexIncorrectException;
import ru.t1.artamonov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) findAll();
        return repository.findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M remove(@Nullable M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}

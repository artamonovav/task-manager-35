package ru.t1.artamonov.tm.repository;

import ru.t1.artamonov.tm.api.repository.ISessionRepository;
import ru.t1.artamonov.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}

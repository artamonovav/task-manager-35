package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

}

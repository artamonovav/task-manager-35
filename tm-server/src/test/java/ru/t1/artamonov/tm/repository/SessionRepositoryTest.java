package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.ISessionRepository;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Session;

import static ru.t1.artamonov.tm.constant.SessionTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Test
    public void add() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clear() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.clear();
        final Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getSize());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER1_SESSION2);
        sessionRepository.clear(USER1.getId());
        Assert.assertEquals(0, sessionRepository.getSize(USER1.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertFalse(sessionRepository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionRepository.existsById(USER1_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertFalse(sessionRepository.existsById(USER1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionRepository.existsById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll(USER1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertNull(sessionRepository.findOneById(NON_EXISTING_SESSION_ID));
        sessionRepository.add(USER1_SESSION1);
        @Nullable final Session session = sessionRepository.findOneById(USER1_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER1_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNotEquals(USER2_SESSION1, sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void getSize() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getSize());
        sessionRepository.add(ADMIN1_SESSION1);
        count = 1;
        Assert.assertEquals(count, sessionRepository.getSize());
    }

    @Test
    public void getSizeByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertEquals(0, sessionRepository.getSize(ADMIN.getId()));
        sessionRepository.add(ADMIN1_SESSION1);
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(1, sessionRepository.getSize(ADMIN.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        @Nullable final Session removedSession = sessionRepository.remove(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeAll() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        sessionRepository.removeAll(SESSION_LIST);
        Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getSize());
    }

    @Test
    public void removeByUserId() {
        Assert.assertNull(sessionRepository.remove(ADMIN.getId(), null));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        Assert.assertNull(sessionRepository.remove(null, createdSession));
        @Nullable final Session removedSession = sessionRepository.remove(ADMIN.getId(), createdSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN.getId(), ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNull(sessionRepository.removeById(null));
        Assert.assertNull(sessionRepository.removeById(NON_EXISTING_SESSION_ID));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        @Nullable final Session removedSession = sessionRepository.removeById(ADMIN1_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertNull(sessionRepository.removeById(ADMIN.getId(), null));
        Assert.assertNull(sessionRepository.removeById(ADMIN.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(sessionRepository.removeById(ADMIN.getId(), USER1_SESSION1.getId()));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        Assert.assertNull(sessionRepository.removeById(null, createdSession.getId()));
        @Nullable final Session removedSession = sessionRepository.removeById(ADMIN.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN.getId(), ADMIN1_SESSION1.getId()));
    }

    @Test
    public void set() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.set(USER2_SESSION_LIST);
        Assert.assertEquals(USER2_SESSION_LIST, sessionRepository.findAll());
    }

}

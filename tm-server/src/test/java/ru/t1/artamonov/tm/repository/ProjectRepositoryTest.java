package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.marker.UnitCategory;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    IProjectRepository projectRepository = new ProjectRepository();

    @Test
    public void add() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), projectRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectRepository.findAll());
        projectRepository.clear(USER2.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.clear(USER1.getId());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER2_PROJECT1);
        projectRepository.clear(USER1.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectRepository.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertNotEquals(USER2_PROJECT1, projectRepository.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.remove(USER1.getId(), USER1_PROJECT1));
        Assert.assertFalse(projectRepository.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectRepository.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.removeById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectRepository.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
    }

}

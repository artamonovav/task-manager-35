package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.service.PropertyService;
import ru.t1.artamonov.tm.util.HashUtil;

import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void add() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userRepository.findAll());
    }

    @Test
    public void set() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        userRepository.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, userRepository.findAll());
    }

    @Test
    public void clear() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.removeById(USER1.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(userRepository.existsById(USER2.getId()));
    }

    @Test
    public void createLoginPassword() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create(propertyService, UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertNull(user.getEmail());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createLoginPasswordEmail() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create(propertyService, UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, UNIT_TEST_USER_EMAIL);
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(UNIT_TEST_USER_EMAIL, user.getEmail());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createLoginPasswordRole() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create(propertyService, UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, Role.USUAL);
        Assert.assertTrue(userRepository.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create(propertyService, UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull User user = userRepository.create(propertyService, UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, UNIT_TEST_USER_EMAIL);
        Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
    }


    @Test
    public void isLoginExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setLogin(UNIT_TEST_USER_LOGIN);
        Assert.assertTrue(userRepository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userRepository.isLoginExist(UNIT_TEST_INCORRECT_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setEmail("test@test.ru");
        Assert.assertTrue(userRepository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userRepository.isEmailExist(UNIT_TEST_USER_EMAIL));
    }

}

package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.api.repository.ITaskRepository;
import ru.t1.artamonov.tm.api.service.IProjectTaskService;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.repository.ProjectRepository;
import ru.t1.artamonov.tm.repository.TaskRepository;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.TaskTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER1_PROJECT2);
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER1_TASK2);
    }

    @After
    public void after() {
        taskRepository.removeAll(TASK_LIST);
        projectRepository.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), USER1_TASK1.getProjectId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT2.getId(), USER1_TASK2.getId());
        Assert.assertEquals(USER1_PROJECT2.getId(), USER1_TASK2.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        bindTaskToProject();
        Assert.assertNotNull(USER1_TASK1.getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        bindTaskToProject();
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
    }

}

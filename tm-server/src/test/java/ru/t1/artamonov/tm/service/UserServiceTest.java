package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.api.repository.ITaskRepository;
import ru.t1.artamonov.tm.api.repository.IUserRepository;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.api.service.IUserService;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.repository.ProjectRepository;
import ru.t1.artamonov.tm.repository.TaskRepository;
import ru.t1.artamonov.tm.repository.UserRepository;
import ru.t1.artamonov.tm.util.HashUtil;

import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Test
    public void add() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertEquals(USER1, userRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userRepository.findAll());
    }

    @Test
    public void set() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST1);
        userService.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, userService.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userService.findAll());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST1);
        Assert.assertEquals(USER1, userService.findOneById(USER1.getId()));
        Assert.assertNotEquals(ADMIN, userService.findOneById(USER1.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.remove(USER1);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeById(USER1.getId());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void createLoginPassword() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void createLoginPasswordEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, UNIT_TEST_USER_EMAIL);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(UNIT_TEST_USER_EMAIL, user.getEmail());
    }

    @Test
    public void createLoginPasswordRole() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, Role.USUAL);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals(UNIT_TEST_USER_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD, UNIT_TEST_USER_EMAIL);
        Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setLogin(UNIT_TEST_USER_LOGIN);
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userService.isLoginExist(UNIT_TEST_INCORRECT_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setEmail(UNIT_TEST_USER_EMAIL);
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userService.isEmailExist("test@test.ru"));
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setLogin(UNIT_TEST_USER_LOGIN);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeByLogin(USER1.getLogin());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void removeByEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setEmail(UNIT_TEST_USER_EMAIL);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeByEmail(USER1.getEmail());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void setPassword() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, "P@SSW0RD");
        userService.setPassword(user.getId(), UNIT_TEST_USER_PASSWORD);
        Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        userService.updateUser(user.getId(),"Firstname", "Lastname", "Middlename");
        Assert.assertEquals("Firstname", user.getFirstName());
        Assert.assertEquals("Lastname", user.getLastName());
        Assert.assertEquals("Middlename", user.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull User user = userService.create(UNIT_TEST_USER_LOGIN, UNIT_TEST_USER_PASSWORD);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(user.getLocked());
    }

}

package ru.t1.artamonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.model.User;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User ADMIN = new User();

    @NotNull
    public final static List<User> USER_LIST1 = Arrays.asList(USER1, USER2, ADMIN);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER2, ADMIN);

    @NotNull
    public final static String UNIT_TEST_USER_LOGIN = "UNIT_TEST_USER";

    @NotNull
    public final static String UNIT_TEST_USER_PASSWORD = "UNIT_TEST_PASSWORD";

    @NotNull
    public final static String UNIT_TEST_USER_EMAIL = "unit_test@email.test";

    @NotNull
    public final static String UNIT_TEST_INCORRECT_LOGIN = "INCORRECT_LOGIN";

}

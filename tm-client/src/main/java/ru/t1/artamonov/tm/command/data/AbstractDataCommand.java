package ru.t1.artamonov.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.artamonov.tm.command.AbstractCommand;
import ru.t1.artamonov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    protected IDomainEndpoint getDomainEndpointClient() {
        return getServiceLocator().getDomainEndpoint();
    }
}

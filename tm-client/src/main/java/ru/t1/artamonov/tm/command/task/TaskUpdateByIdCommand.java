package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "Update task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        request.setTaskId(id);
        getTaskEndpointClient().updateById(request);
    }

}

package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectGetByIdRequest(@Nullable String token) {
        super(token);
    }

}


package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

@NoArgsConstructor
public class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(@Nullable final Task task) {
        super(task);
    }

}

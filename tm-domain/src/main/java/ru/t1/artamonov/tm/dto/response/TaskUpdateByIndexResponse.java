package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

@NoArgsConstructor
public class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
